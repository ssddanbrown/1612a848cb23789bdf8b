var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');
var sourcemaps = require('gulp-sourcemaps');
var cleanCss = require('gulp-clean-css');
var ts = require('gulp-typescript');

var paths = {
    cssSrc: 'styles/*.scss',
    cssDist: 'dist/styles',
    tsSrc: 'scripts/*.ts',
    tsDist: 'dist/scripts'
}

gulp.task('sass-dev', function () {
    gulp.src(paths.cssSrc)
      .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
          browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.cssDist))
        .pipe(livereload());
});

gulp.task('sass', function () {
    gulp.src(paths.cssSrc)
        .pipe(sass())
        .pipe(autoprefixer({
          browsers: ['last 2 versions']
        }))
        .pipe(cleanCss())
        .pipe(gulp.dest(paths.cssDist))
});

gulp.task('typescript-dev', function () {
    return gulp.src(paths.tsSrc)
        .pipe(sourcemaps.init())
        .pipe(ts({
            noImplicitAny: true,
            target: 'ES5',
            sortOutput: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.tsDist));
});

gulp.task('typescript', function () {
    return gulp.src(paths.tsSrc)
        .pipe(ts({
            noImplicitAny: true,
            target: 'ES5',
            sortOutput: true
        }))
        .pipe(gulp.dest(paths.tsDist));
});

gulp.task('default', ['sass-dev', 'typescript-dev']);
gulp.task('production', ['sass', 'typescript']);

gulp.task('watch', ['sass-dev'], function() {
	livereload.listen();
    gulp.watch(['*.php', '*.html', '*.js']).on('change', livereload.changed);
	gulp.watch('styles/**', ['sass-dev']);
    gulp.watch('scripts/*.ts', ['typescript-dev']);
});